#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>

#define CACHE_LINE_SIZE_BYTES 64

typedef int *(*FillFn)(int *, int);

int rand_range(int lower_inclusive, int upper_exclusive) {
    return lower_inclusive + rand() / (RAND_MAX / (upper_exclusive - lower_inclusive) + 1); // NOLINT
}

void swap(int *a, int *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void array_print(const int *array, int size) {
    if (NULL == array) {
        fprintf(stderr, "array is NULL\n");
        return;
    }

    printf("[");

    for (int i = 0; i < size - 1; i += 1) {
        printf("%d, ", array[i]);
    }

    printf("%d", array[size - 1]);
    printf("]\n");
}

int *sattolo_cycle(int *array, int size) {
    if (NULL == array) {
        fprintf(stderr, "array is NULL\n");
        return NULL;
    }

    int i = size;
    while (i > 1) {
        i -= 1;
        int j = rand_range(0, i);
        swap(array + i, array + j);
    }

    return array;
}

int *array_new(int size) {
    if (size < 1) {
        fprintf(stderr, "invalid size: %d\n", size);
        return NULL;
    }

    int *array = (int *) aligned_alloc(CACHE_LINE_SIZE_BYTES, sizeof(*array) * size);
    if (NULL == array) {
        perror("cannot allocate memory");
        return NULL;
    }

    return array;
}

void array_free(int **array) {
    if (NULL == array) {
        fprintf(stderr, "pointer to array is NULL\n");
        return;
    }

    free(*array);
    *array = NULL;
}

int *array_fill_forward(int *array, int size) {
    if (NULL == array) {
        fprintf(stderr, "array is NULL\n");
        return NULL;
    }

    for (int i = 0; i < size; i += 1) {
        array[i] = (i + 1) % size;
    }

    return array;
}

int *array_fill_backward(int *array, int size) {
    if (NULL == array) {
        fprintf(stderr, "array is NULL\n");
        return NULL;
    }

    for (int i = 0; i < size; i += 1) {
        array[i] = (size + i - 1) % size;
    }

    return array;
}

int *array_fill_indices(int *array, int size) {
    if (NULL == array) {
        fprintf(stderr, "array is NULL\n");
        return NULL;
    }

    for (int i = 0; i < size; i += 1) {
        array[i] = i;
    }

    return array;
}

int *array_fill_random(int *array, int size) {
    if (NULL == array) {
        fprintf(stderr, "array is NULL\n");
        return NULL;
    }

    return sattolo_cycle(array_fill_indices(array, size), size);
}

int *array_fill_random_cache_line(int *array, int size) {
    if (NULL == array) {
        fprintf(stderr, "array is NULL\n");
        return NULL;
    }

    const int elements_per_line = CACHE_LINE_SIZE_BYTES / sizeof(*array);
    const int lines_count = size / elements_per_line;

    int *cache_line_index = array_new(lines_count);
    cache_line_index = array_fill_forward(cache_line_index, lines_count);
    cache_line_index = sattolo_cycle(cache_line_index, lines_count);
    if (NULL == cache_line_index) {
        return NULL;
    }

    for (int i = 0; i < lines_count; i += 1) {
        const int line_start = cache_line_index[i] * elements_per_line;
        const int next_line_start = cache_line_index[(i + 1) % lines_count] * elements_per_line;

        int *line = array + line_start;

        for (int j = 0; j < elements_per_line; j += 1) {
            line[j] = line_start + j + 1;
        }

        line[elements_per_line - 1] = next_line_start;

    }

    array_free(&cache_line_index);

    return array;
}

int *array_fill_forward_stride(int *array, int size) {
    if (NULL == array) {
        fprintf(stderr, "array is NULL\n");
        return NULL;
    }

    const int stride = CACHE_LINE_SIZE_BYTES / sizeof(*array);

    for (int i = 0; i < size; i += 1) {
        const int next_index = i + stride;
        array[i] = next_index < size ? next_index : (next_index % size + 1);
    }

    array[size - 1] = 0;

    return array;
}

int array_walk(const int *array, int size, int repeats) {
    if (NULL == array) {
        fprintf(stderr, "array is NULL\n");
        return -1;
    }

    register int next_index = 0;
    for (int i = 0; i < size * repeats; ++i) {
        next_index = array[next_index];
    }

    return array[next_index];
}

static inline uint64_t rdtsc() {
    register uint64_t rax, rdx;

    asm volatile ( "rdtscp\n" : "=a" (rax), "=d" (rdx));

    return (rdx << 32u) + rax;
}

void print_size(int size_bytes) {
    const int mb = 1024 * 1024;
    const int kb = 1024;

    if (size_bytes >= mb) {
        if (size_bytes % mb > 0) {
            return (void) printf("%.2lfMiB", (double) size_bytes / mb);
        }

        return (void) printf("%dMiB", size_bytes / mb);
    }

    if (size_bytes >= kb) {
        if (size_bytes % kb > 0) {
            return (void) printf("%.2lfKiB", (double) size_bytes / kb);
        }

        return (void) printf("%dKiB", size_bytes / kb);
    }

    return (void) printf("%dB", size_bytes);
}

static inline uint64_t min_uint64_t(uint64_t a, uint64_t b) {
    return a < b ? a : b;
}

void measure_access_time(int *(*fill)(int *, int), int size_bytes, int n_repeats) {
    int size = size_bytes / (int) sizeof(int);
    int *array = array_new(size);
    if (NULL == array) {
        exit(EXIT_FAILURE);
    }

    fill(array, size);

    volatile int discard = 0;
    discard += array_walk(array, size, 1);
    printf("%d\r", discard);

    const int global_repeats = 10;
    uint64_t min_time = ~0u;

    for (int i = 0; i < global_repeats; i += 1) {
        char s[32];
        volatile uint64_t start = rdtsc();
        sprintf(s, "%d\r", (int) start);
        discard += array_walk(array, size, n_repeats);
        sprintf(s, "%d\r", (int) discard);
        volatile uint64_t end = rdtsc();
        sprintf(s, "%d\r", (int) end);

        const uint64_t access_time = (end - start) / (uint64_t) n_repeats / (uint64_t) size;
        min_time = min_uint64_t(min_time, access_time);
    }

    printf("%d\r", discard);

    printf("%lu\n", min_time);

    array_free(&array);
}

void print_sizes(int min_size_b, int max_size_b) {
    printf("Size\n");

    for (int size_bytes = min_size_b; size_bytes <= max_size_b;) {
        const int base_size = size_bytes;

        for (int n = 0; n < 4; n += 1) {
            size_bytes = base_size + n * base_size / 4;

            if (size_bytes > max_size_b) {
                break;
            }

            print_size(size_bytes);
            printf("\n");
        }

        size_bytes = base_size * 2;
    }
}

void measure_access_time_sizes(
        const char *header,
        int min_size_b, int max_size_b,
        FillFn fill,
        int n_repeats
) {
    printf("%s\n", header);

    for (int size_bytes = min_size_b; size_bytes <= max_size_b;) {
        const int base_size = size_bytes;

        for (int n = 0; n < 4; n += 1) {
            size_bytes = base_size + n * base_size / 4;

            if (size_bytes > max_size_b) {
                break;
            }

            measure_access_time(fill, size_bytes, n_repeats);
        }

        size_bytes = base_size * 2;
    }
}

int main(int argc, char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Missing parameter N_REPEATS\n");
        return EXIT_FAILURE;
    }
    const int n_repeats = (int) strtol(argv[1], NULL, 10);
    if (errno || n_repeats <= 0) {
        fprintf(stderr, "Invalid parameter: \"%s\"\n", argv[1]);
        return EXIT_FAILURE;
    }

    const int min_size_b = 1024;
    const int max_size_b = 32 * 1024 * 1024;

    print_sizes(min_size_b, max_size_b);

    const FillFn fill_methods[] = {
            array_fill_forward,
            array_fill_backward,
            array_fill_random,
            array_fill_random_cache_line,
            array_fill_forward_stride
    };
    const char *fill_method_names[] = {
            "Forward",
            "Backward",
            "Random",
            "Random (line)",
            "Forward (stride)"
    };
    const int n_methods = sizeof(fill_method_names) / sizeof(*fill_method_names);

    for (int i = 0; i < n_methods; i += 1) {
        measure_access_time_sizes(fill_method_names[i], min_size_b, max_size_b, fill_methods[i], n_repeats);
    }
    return EXIT_SUCCESS;
}
